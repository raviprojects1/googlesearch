package seleniumgluecode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;




public class Hook {
	
	public static final String TEST_NAME = "testname";
	
	@Before
	public static void BeforeScenario(Scenario scenario) {
		
		System.out.println("Before executing sceario :"+ scenario.getName());
		MDC.put(TEST_NAME, scenario.getName() );
		
	}
	
	
	@After
	public static void AfterScenario(Scenario scenario)
	{
		System.out.println("After executing sceario");
		MDC.remove(TEST_NAME);
	}

}
