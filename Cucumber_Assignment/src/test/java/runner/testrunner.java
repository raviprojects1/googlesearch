package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/test/javaFeatures"
,glue= {"seleniumgluecode"},
plugin = {
		 "json:target/cucumber-reports/cucumber.json",
		 "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"}
)

public class testrunner {

}
